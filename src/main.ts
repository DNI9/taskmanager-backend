import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);
  const PORT = process.env.PORT || 5000;

  // app.enableCors({ origin: process.env.FRONTEND_URL });
  app.enableCors();
  await app.listen(PORT);
  logger.log(`Application listening on port ${PORT}`);
}
bootstrap();
